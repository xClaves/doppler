function [ output_args ] = displayTF(signal,fe,time)
[m,n] = size(signal);
t = linspace(0,1,n);
figure;
plot(t,signal);
xlabel('Temps (secondes)')
ylabel('signal(t)')
S = fft(signal,n);
% frequence
dF = 1 / time;
f = -fe/2:dF:fe/2-dF;

figure;
disp('f' + size(f));
res = fftshift(abs(S));
plot(f,res);
xlabel('Frequence (hertz)')
ylabel('Module')
end

