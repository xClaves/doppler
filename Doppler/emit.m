function [ signal ] = emit(f,fe,time)
%EMIT Summary of this function goes here
%   Detailed explanation goes here

nbEch = fe*time;
t = linspace(0,1,nbEch);
signal = cos(2*pi*f*t);
end

