function [ mod ] = demodulation( signal,f0,time )
[m,n] = size(signal);
e = emit(f0,n/time,time);
mod = e .* signal;

end

