% 
% function [v] = fluidSpeed(fd, theta, c )
% 
% % IF v<<c
% % Taylor developpement gives the following formula
% v = fd*c/(f0*2*cos(theta));
% 
% end

% 1. 
fe = 10*10^6; % 10MHz
f0 = 2*10^6; % 2MHz
fd = 5 * 10^3; %5Khz
emitTime = 0.001;

signal = emit(f0 + fd,fe, emitTime);
displayTF(signal,fe,emitTime);

%estimation par re-échantillonage
mod = demodulation(signal,f0,emitTime);
displayTF(mod,f0,emitTime);

% Nous avons l'apparition de deux pics caractéristiques d'un signal
% sinusoidal, ces pics se trouvent à la fréquence suivante :
[value, index] = max(fftshift(abs(fft(mod))));
disp(abs(f(index))); %fd =~ 5.4973 * 10^3


