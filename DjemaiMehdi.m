%% Moindres carres
% 1.
t = 1:19/1900:20;
signal = 1 + log10(t);
Ps = mean(signal.*signal);
% SNRdB = 8 = 20 * log10( Ps / Pb )
% 8 = 20 log10(Ps/pb)
% 10^(8/20) = Ps/Pb
% Pb = Ps/(10^(8/20))
% Pour le bruit gaussien Pb = sigma^2
sigma = sqrt(Ps/(10^(8/20)));
Y=signal+sigma*randn(size(t));
%plot(Y);

% 2.
% A = 2x1
% E = 1901x1
% donc U = 1901x2
a1 = 1;
a2 = 1;
E = sigma*randn(size(t))';
A = [a1 a2]';
U = [ones(size(t));log10(t)]';
Y = U*A + E;
%figure;
%plot(Y);

% 3.
A_est = inv(U.'*U)*U.'*Y;
disp(A_est);

% 4.
figure;
hold on;
plot(Y);
plot(U*A_est, 'LineWidth',2);
plot(signal, 'LineWidth',1);
legend('Signal bruite', 'Signal estime', 'Signal non bruite');

%% Estimation de la fr�quence Doppler
% 1. 
fe = 10*10^6; % 10MHz
f0 = 2*10^6; % 2MHz
t = 0:1/fe:10^-3;
e = cos(2 * pi * f0 * t);
plot(t,e);
xlabel('Temps (secondes)')
ylabel('e(t)')
E = fft(e);
% frequence
dF = fe/size(t,2);
f = -fe/2:dF:fe/2-dF;
figure;
plot(f, fftshift(abs(E)));
xlabel('Frequence (hertz)')
ylabel('Module')
% il y a deux pics sym�triques correspondants � la transform�e de
% fourier d'un cosinus (et � la fr�quence 2MHz)

% 2.
figure;
fd = 5 * 10^3; %5Khz
s = cos(2 * pi * (f0 + fd) * t);
plot(t,s);
xlabel('Temps (secondes)')
ylabel('s(t)')
S = fft(s);
% frequence
dF = fe/size(t,2);
f = -fe/2:dF:fe/2-dF;
figure;
plot(f, fftshift(abs(S)));
xlabel('Frequence (hertz)')
ylabel('Module')
% On remarque qu'il y a toujours les deux pics sym�triques autour de 2Mhz,
% on remarque cependant qu'ils sont l�g�rement d�cal�s compar�s au signal
% e(t), ce qui est logique car on utilise f0+fd avec fd tr�s petit par rapport � f0.

% 3.a.
sd = s.*e;
SD = fft(sd);
% frequence
dF = fe/size(t,2);
f = -fe/2:dF:fe/2-dF;
figure;
plot(f, fftshift(abs(SD)));
xlabel('Frequence (hertz)')
ylabel('Module')
% il y a deux pics � + et - 5kHz qui repr�sentent la fr�quence fd, elle a bien �t�
% isol�e, par contre la fr�quence du signal principal f0 a �t� d�cal� �
% 4Mhz au lieu de 2Mhz

% 3.a.i.
% Nous pouvons filter le signal avec un filtre passe bas de fc � 1MHz par
% exemple, ce qui nous permet d'isoler le signal de fr�quence 5kHz, il
% suffit alors de calculer la densit� spectrale de puissance et le max nous
% donne la fr�quence fd

% d = fdesign.lowpass('Fp,Fst,Ap,Ast',1*10^6,2*10^6,0.5,40,fe);
% Hd = design(d,'equiripple');
% output = filter(Hd,sd);
% Pxx = pwelch(output, hamming(32), 16);
% plot((1:129)*fe, 10*log10(Pxx));

% on remarque aussi que dans la fft c'est un max, d'o�
[value, index] = max(fftshift(abs(SD)));
disp(abs(f(index))); %5.4995 * 10^3

% 3.b
fe = f0;
t = 0:1/fe:10^-3;
s = cos(2 * pi * (f0 + fd) * t);
figure;
plot(t,s);
xlabel('Temps (secondes)')
ylabel('e(t)')
S = fft(s);
% frequence
dF = fe/size(t,2);
f = -fe/2:dF:fe/2-dF;
figure;
plot(f, fftshift(abs(S)));
xlabel('Frequence (hertz)')
ylabel('Module')
% Nous avons l'apparition de deux pics caract�ristiques d'un signal
% sinusoidal, ces pics se trouvent � la fr�quence suivante :
[value, index] = max(fftshift(abs(S)));
disp(abs(f(index))); %fd =~ 5.4973 * 10^3